const sha256 = require("sha256");
const currentNodeUrl = process.argv[3];

// Constructor function
function Blockchain() {
  this.chain = []; // the majority of the block will be stored here
  this.pendingTransactions = [];

  this.currentNodeUrl = currentNodeUrl;
  this.networkNodes = [];

  this.createNewBlock(100, "0", "0"); // creating Genisis block
}

// Method for the Blockchain
Blockchain.prototype.createNewBlock = function(nonce, previousBlockHash, hash) {
  // create new block object
  const newBlock = {
    index: this.chain.length + 1, //block # in the chain
    timestamp: Date.now(), // timestamp
    transactions: this.pendingTransactions,
    nonce: nonce, // a proof that created a new block in a legit way
    hash: hash, // data from the new current block
    previousBlockHash: previousBlockHash // data from previous block
  };

  this.pendingTransactions = [];
  this.chain.push(newBlock); // takes new block and pushes to chain

  return newBlock;
};

Blockchain.prototype.getlastBlock = function() {
  return this.chain[this.chain.length - 1];
};

Blockchain.prototype.createNewTransaction = function(
  amount,
  sender,
  recipient
) {
  const newTransaction = {
    amount: amount,
    sender: sender,
    recipient: recipient
  };

  this.pendingTransactions.push(newTransaction);
  /* Once a new block is formed pending transactions become recorded transactions
    and cannot be changed */

  return this.getlastBlock()["index"] + 1; // index of last block of chain + 1
};

Blockchain.prototype.hashBlock = function(
  previousBlockHash,
  currentBlockData,
  nonce
) {
  // return fixed length string '3423ADADKASDKADASDAS' etc
  const dataAsString =
    previousBlockHash + nonce.toString() + JSON.stringify(currentBlockData);
  const hash = sha256(dataAsString);

  return hash;
};

Blockchain.prototype.proofOfWork = function(
  previousBlockHash,
  currentBlockData
) {
  // => repeatedly hash block until it finds correct hash => '5ef7b6028ab14dad452b525ecc0c3efcb958193127ed7983be48769f08207c41'
  // => use current block data for the hash, but also the previousBlockHash
  // => continuously change nonce value until it finds the corect hash
  // => returns to us the nonce value that creates the correct hash

  let nonce = 0;
  let hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);

  // This is the part where it uses teh compute power from video cards
  // if hash doesn't hash doesnt start with four 0's it will generate a new hash,
  // it will continue until it finds a hash with four 0's
  while (hash.substring(0, 4) !== "0000") {
    nonce++;
    hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);
    //console.log(hash);
  }

  return nonce; // nonce is pretty much the proof
};

module.exports = Blockchain;
