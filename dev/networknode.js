const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const blockchain = require("./blockchain");
const uuid = require("uuid/v1");
const port = process.argv[2];
const reqprom = require("request-promise");

// remove dashes from uuid and join together
const nodeAddress = uuid()
  .split("-")
  .join("");

const nerndcoin = new blockchain();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.get("/", function(req, res) {
  res.send("Hello World");
});

app.get("/blockchain", function(req, res) {
  res.send(nerndcoin);
});

app.post("/transaction", function(req, res) {
  const blockindex = nerndcoin.createNewTransaction(
    req.body.amount,
    req.body.sender,
    req.body.recipient
  );

  res.json({ note: `Transaction will be added in block ${blockindex} ` });
  //res.send(`the amout of the transaction is ${req.body.amount} NERnDCoin`);
});

app.get("/mine", function(req, res) {
  const lastBlock = nerndcoin.getlastBlock();
  const previousBlockHash = lastBlock["hash"];
  const currentBlockData = {
    transactions: nerndcoin.pendingTransactions,
    index: lastBlock["index"] + 1
  };

  const nonce = nerndcoin.proofOfWork(previousBlockHash, currentBlockData);
  const blockHash = nerndcoin.hashBlock(
    previousBlockHash,
    currentBlockData,
    nonce
  );

  // Reward for mining new block
  // As a sender we're putting a 00 that is a mining reward
  nerndcoin.createNewTransaction(130, "00", nodeAddress);

  const newBlock = nerndcoin.createNewBlock(
    nonce,
    previousBlockHash,
    blockHash
  );

  res.json({
    note: "Success! The block got mined",
    block: newBlock
  });
});

//register node and broadcast it to the network
app.post("/register-broadcast-node", function(req, res) {
  const newNodeUrl = req.body.newNodeUrl;
  if (nerndcoin.networkNodes.indexOf(newNodeUrl) == -1)
    nerndcoin.networkNodes.push(newNodeUrl);

  const registerNodesPromises = [];
  //broadcast
  nerndcoin.networkNodes.forEach(networkNodeUrl => {
    // register-node endpoint
    const requestOptions = {
      uri: networkNodeUrl + "/register-node",
      method: "POST",
      body: { newNodeUrl: newNodeUrl },
      json: true
    };
    registerNodesPromises.push(reqprom(requestOptions));
  });

  Promise.all(registerNodesPromises)
    .then(data => {
      // use data...
      const bulkRegisterOptions = {
        url: newNodeUrl + "/register-nodes-bulk",
        method: "POST",
        body: {
          allNetworkNodes: [...nerndcoin.networkNodes, nerndcoin.currentNodeUrl]
        },
        json: true
      };

      return reqprom(bulkRegisterOptions);
    })
    .then(data => {
      res.json({ note: "Succes! New node registered with network" });
    })
    .catch(function(err) {
      console.error(err); // This will print any error that was thrown in the previous error handler.
    });

  // calculations and broadcast
});

// register a node with the network
app.post("/register-node", function(req, res) {
  // ...
  const newNodeUrl = req.body.newNodeUrl;
  const nodeNotPresent = nerndcoin.networkNodes.indexOf(newNodeUrl) == -1;
  const nodeNotCurrent = nerndcoin.currentNodeUrl !== newNodeUrl;
  if (nodeNotPresent && nodeNotCurrent) nerndcoin.networkNodes.push(newNodeUrl); // This registers the node URL into the nodes network nodes array
  res.json({ note: "New Node Registered Successfully" });
});

app.post("/register-nodes-bulk", function(req, res) {
  const allNetworkNodes = req.body.allNetworkNodes;

  //loop through every node url in the array and register it in the new node
  allNetworkNodes.forEach(networkNodeUrl => {
    // as the cycle goes we are registering each one by pushing that networkNode URL into the networkNodes array
    const nodeNotAlreadyPresent =
      nerndcoin.networkNodes.indexOf(networkNodeUrl) == -1;
    const notCurrentNode = nerndcoin.currentNodeUrl !== networkNodeUrl;
    if (nodeNotAlreadyPresent && notCurrentNode)
      nerndcoin.networkNodes.push(networkNodeUrl);
  });

  res.json({ note: "Bulk registration is a success" });
});

app.listen(port, function() {
  console.log(`Listening on ${port} ...`);
});
