const Blockchain = require("./blockchain");

const nerndcoin = new Blockchain();

nerndcoin.createNewBlock(09090, "AFDSFSDFSDFSDF", "WERWERWER"); // Genisis Block

// pending transaction created when block below is mined
nerndcoin.createNewTransaction(102, "JOHNSDAD", "JANETERTERTERT");

nerndcoin.createNewBlock(12121, "LJKLKLKLK", "KJHHJHJKHJ"); // mine a block

nerndcoin.createNewTransaction(101, "JOHNSDAD", "JANETERTERTERT");
nerndcoin.createNewTransaction(155, "JOHNSDAD", "JANETERTERTERT");
nerndcoin.createNewTransaction(145, "JOHNSDAD", "JANETERTERTERT");

nerndcoin.createNewBlock(12123, "AAAAA", "BBBBBB"); // mine a block

console.log(nerndcoin.chain[2]);
